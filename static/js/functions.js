(function ($) {
    'use strict';

    // Slider Activate
    if ($.fn.owlCarousel) {
        $(".testimonials").owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ['<i class="pe-7s-angle-left" aria-hidden="true"></i>', '<i class="pe-7s-angle-right" aria-hidden="true"></i>'],
            dots: false,
            autoplay: true,
            smartSpeed: 700,
            autoplayTimeout: 5000
        });
    }

	// Slider Activate with Captions (figcaption)
    if ($.fn.owlCarousel) {
		var owl = $('.client_slides');
		owl.owlCarousel({
            items: 3,
            margin: 0,
            loop: true,
            nav: true,
            navText: [
	            '<i class="pe-7s-angle-left">',
	            '<i class="pe-7s-angle-right">'
	        ],
            dots: false,
			autoplay: true,
		    autoplayTimeout: 5000,
		    autoplayHoverPause: true,
            fluidSpeed: 1000,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                576: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            },
        });
	}

    // Onepage Nav Active Code
    if ($.fn.onePageNav) {
        $('header nav ul').onePageNav({
            currentClass: 'current_page_item',
            scrollSpeed: 1500,
            easing: 'easeInOutQuart'
        });
    }

    // Parallax active js
    if ($.fn.jarallax) {
        var isIE = /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/\d+/.test(navigator.userAgent);
        if (!isIE) {
            $('.parallax').jarallax({
                speed: 0.2,
                noIos: true,
            });
        }
    }

    if ($.fn.init) {
        new WOW().init();
    }

    // Counterup Active Code
    if ($.fn.counterUp) {
        $('.counter').counterUp({
            delay: 10,
            time: 2000
        });
    }

    // MatchHeight Active Code
    if ($.fn.matchHeight) {
        $('.item').matchHeight();
    }

    var $window = $(window);
    // Fullscreen Active Code    
    $window.on('resizeEnd', function () {
        $(".full_height").height($window.height());
    });
    $window.on('resize', function () {
        if (this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 300);
    }).trigger("resize");

    // Sticky Active Code
    $window.on('scroll', function () {
        // Fadeout text code
        //$("main .slogan").css("opacity", 1 - $(window).scrollTop() / $('main .slogan').height());        
        
        if ($window.scrollTop() > 90) {
            $('.main').addClass('sticky fadeIn');
            $('body').addClass('mobile_menu_on fullmenu-on');
        } else {
            $('.main').removeClass('sticky fadeIn');
            $('body').removeClass('mobile_menu_on fullmenu-on');
        }
    });

    // If we have a lonely Zeal crew, we give them a partner :D
    const Advisors = [...document.querySelectorAll(".our_advisor_area > div")]
    const Experts = [...document.querySelectorAll(".our_experts_area > div")]

    let wrapWidth = 50;

    Advisors.forEach(e => {
        if ((e.childNodes.length / 2 >> 0) % 5 === 1) {
            e.childNodes[e.childNodes.length - 6].style.marginRight = `${wrapWidth}px`;
        }        
    })

    Experts.forEach(e => {
        if ((e.childNodes.length / 2 >> 0) % 5 === 1) {
            e.childNodes[e.childNodes.length - 6].style.marginRight = `${wrapWidth}px`;
        }        
    })

})(jQuery);